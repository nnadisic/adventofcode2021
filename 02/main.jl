
using DelimitedFiles

commands = readdlm("./input.txt")


function puzzle1(commands)
    # Init variables
    horizontal = 0
    depth = 0

    # Loop on commands
    for com in eachrow(commands)
        direction = com[1]
        value = com[2]
        if direction == "forward"
            horizontal += value
        elseif direction == "down"
            depth += value
        elseif direction == "up"
            depth -= value
        end
    end

    return horizontal * depth
end
# display(puzzle1(commands))


function puzzle2(commands)
    # Init variables
    horizontal = 0
    depth = 0
    aim = 0

    # Loop on commands
    for com in eachrow(commands)
        direction = com[1]
        value = com[2]
        if direction == "forward"
            horizontal += value
            depth += aim * value
        elseif direction == "down"
            aim += value
        elseif direction == "up"
            aim -= value
        end
    end

    return horizontal * depth
end
display(puzzle2(commands))
