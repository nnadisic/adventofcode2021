
function puzzle1(filename::String, nbrow::Int, nbcol::Int) ::Int
    # Parse input
    rawdata = read(open(filename), String)
    rawdots, rawfolds = [split(section) for section in split(rawdata, "\n\n")]
    # Parse dots
    dots = zeros(Int, length(rawdots), 2)
    for i in 1:length(rawdots)
        dots[i,:] .= parse.(Int, split(rawdots[i], ","))
    end
    # Julia arrays begin at index 1
    dots .+= 1
    # Parse folds
    instr = rawfolds[3:3:end]
    folds = [(z[1], parse(Int, z[3:end]) + 1 ) for z in instr]

    # Build paper
    paper = zeros(Int, nbrow, nbcol)
    for dot in eachrow(dots)
        paper[dot[2], dot[1]] += 1
    end

    # Fold
    out1 = -1 # number of visible dots after first fold
    for fold in folds
        direction, coord = fold
        if direction == 'x'
            paper = paper[:,1:coord-1] + reverse(paper[:,coord+1:end], dims=2)
        else # == 'y'
            paper = paper[1:coord-1,:] + reverse(paper[coord+1:end,:], dims=1)
        end
        if out1 == -1
            out1 = count(paper .> 0)
        end
    end

    # Print final state
    println("")
    for i in 1:size(paper,1)
        for j in 1:size(paper,2)
            char = paper[i,j] .> 0 ? '#' : ' '
            print(char)
        end
        println("")
    end

    # Return
    return out1
end
display(puzzle1("./example", 15, 11)) # should give 17
display(puzzle1("./input", 895, 1311))
