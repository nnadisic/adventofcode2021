
function puzzle1(filename)
    # Parse input
    lines = readlines(filename)
    floor = zeros(Int, length(lines), length(lines[1]))
    for (i, l) in enumerate(lines)
        floor[i,:] .= [parse(Int, x) for x in l]
    end

    # Count low points
    sum = 0
    for idx in CartesianIndices(floor)
        i, j = idx[1], idx[2]
        islow = true
        # un peu sale mais bon
        if (i != 1               && floor[idx] >= floor[i-1,j]) ||
            (i != size(floor, 1) && floor[idx] >= floor[i+1,j]) ||
            (j != 1              && floor[idx] >= floor[i,j-1]) ||
            (j != size(floor, 2) && floor[idx] >= floor[i,j+1])
            # then
            islow = false
        end
        if islow
            sum += floor[idx] + 1
        end
    end

    # Return
    return sum
end
# display(puzzle1("./example"))
# display(puzzle1("./input"))


function puzzle2(filename)
    # Parse input
    lines = readlines(filename)
    floor = zeros(Int, length(lines), length(lines[1]))
    for (i, l) in enumerate(lines)
        floor[i,:] .= [parse(Int, x) for x in l]
    end

    # Find low points
    lowpoints = Vector{CartesianIndex}()
    for idx in CartesianIndices(floor)
        i, j = idx[1], idx[2]
        islow = true
        # un peu sale mais bon
        if (i != 1               && floor[idx] >= floor[i-1,j]) ||
            (i != size(floor, 1) && floor[idx] >= floor[i+1,j]) ||
            (j != 1              && floor[idx] >= floor[i,j-1]) ||
            (j != size(floor, 2) && floor[idx] >= floor[i,j+1])
            # then
            islow = false
        end
        if islow
            push!(lowpoints, idx)
        end
    end

    # Find bassins
    sizesbassins = Vector{Int}()
    for lp in lowpoints
        bassin = [lp]
        finalbassin = [lp]
        while !isempty(bassin)
            point = popfirst!(bassin)
            i, j = point[1], point[2]
            if (i != 1 && floor[point] < floor[i-1,j])
                push!(bassin, CartesianIndex(i-1, j))
                push!(finalbassin, CartesianIndex(i-1, j))
            end
            if (i != size(floor, 1) && floor[point] < floor[i+1,j])
                push!(bassin, CartesianIndex(i+1, j))
                push!(finalbassin, CartesianIndex(i+1, j))
            end
            if (j != 1 && floor[point] < floor[i,j-1])
                push!(bassin, CartesianIndex(i, j-1))
                push!(finalbassin, CartesianIndex(i, j-1))
            end
            if (j != size(floor, 2) && floor[point] < floor[i,j+1])
                push!(bassin, CartesianIndex(i, j+1))
                push!(finalbassin, CartesianIndex(i, j+1))
            end
        end
        # Remove 9's
        filter!(idx->(floor[idx]!=9), finalbassin)
        # Remove duplicates and push
        push!(sizesbassins, length(unique(finalbassin)))
    end

    # Return
    # display(sizesbassins)
    return prod(sort(sizesbassins)[end-2:end])
end
display(puzzle2("./example"))
display(puzzle2("./input"))
