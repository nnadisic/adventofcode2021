using StatsBase # for countmap

function puzzle(filename::String, nbsteps::Int) ::Int
    # Parse
    lines = readlines(filename)
    template = [only(c) for c in lines[1]]
    rawrules = lines[3:end]
    rules = Dict{Tuple{Char,Char}, Char}()
    for rr in rawrules
        left, right = split(rr, " -> ")
        c1, c2 = [only(c) for c in left]
        rules[(c1, c2)] = only(right)
    end

    # Init counters
    letters = countmap(template)
    pairs = countmap([(l1, l2) for (l1, l2) in zip(template[1:end-1], template[2:end])])

    for step in 1:nbsteps
        newpairs = copy(pairs)
        for rule in rules
            (l1, l2) = rule[1]
            char = rule[2]
            nbcurrule = get(pairs, (l1, l2), 0)
            if !iszero(nbcurrule)
                # Add the new char in letters counter
                if haskey(letters, char)
                    letters[char] += nbcurrule
                else
                    letters[char] = nbcurrule
                end
                # Add the 2 new pairs in pairs counter
                if haskey(newpairs, (l1, char))
                    newpairs[(l1, char)] += nbcurrule
                else
                    newpairs[(l1, char)] = nbcurrule
                end
                if haskey(newpairs, (char, l2))
                    newpairs[(char, l2)] += nbcurrule
                else
                    newpairs[(char, l2)] = nbcurrule
                end
                # Remove old pair
                newpairs[(l1, l2)] -= nbcurrule
            end
        end
        pairs = copy(newpairs)
    end

    return findmax(letters)[1] - findmin(letters)[1]
end
display(puzzle("example", 10))
display(puzzle("input", 10))
display(puzzle("input", 40))

# 1588
# 2851
# 10002813279337
