
function puzzle1(filename)
    # Parse data
    data = split(read(filename, String), ",")
    population = [parse(Int, x) for x in data]

    # Simulate
    for day in 1:80
        for i in 1:length(population)
            fish = @view population[i]
            if iszero(fish)
                push!(population, 8)
                fish .= 6
            else
                fish .-= 1
            end
        end
    end

    return length(population)
end
# display(puzzle1("./example"))
# display(puzzle1("./input"))


using Printf

function puzzle2(filename)
    # Parse data
    data = split(read(filename, String), ",")
    population = [parse(Int, x) for x in data]
    fishes = zeros(9)
    for i in 1:9
        fishes[i] = count(==(i-1), population)
    end

    # Simulate
    for day in 1:256
        newfishes = zeros(BigInt, 9)
        newfishes[9] = fishes[1] # babies
        newfishes[1:8] .= fishes[2:9] # time decrease for existing fishes
        newfishes[7] += fishes[1] # reset of fishes that just had a baby
        fishes .= newfishes
    end

    return convert(BigInt, sum(fishes))
end
display(puzzle2("./example"))
display(puzzle2("./input"))
