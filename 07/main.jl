using Statistics

function puzzle1(filename)
    # Parse data
    data = split(read(filename, String), ",")
    crabs = parse.(Int, data)
    # Compute and return
    return sum([abs(crab - median(crabs)) for crab in crabs])
end
display(puzzle1("./example"))
display(puzzle1("./input"))


function puzzle2(filename)
    # Parse data
    data = split(read(filename, String), ",")
    crabs = parse.(Int, data)
    crabs .+= 1 # Julia arrays start at 1 (does not change distances)
    maxcrab = maximum(crabs)

    # Map distances to fuel consumption (compute once and for all)
    disttofuel = [sum(1:i) for i in 1:maxcrab]
    # Add zero distance
    pushfirst!(disttofuel, 0)

    # Brute force
    bestfuel = Inf
    for pos in 1:maxcrab
        fuel = sum([disttofuel[abs(crab - pos) + 1] for crab in crabs])
        bestfuel = min(fuel, bestfuel)
    end

    # Compute and return
    return convert(BigInt, bestfuel)
end
display(puzzle2("./example"))
display(puzzle2("./input"))
