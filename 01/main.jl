
using DelimitedFiles

numbers = readdlm("./input.txt")

function puzzle1(numbers)
    nbincreased = 0
    for i in 1:length(numbers)-1
        if numbers[i] < numbers[i+1]
            nbincreased += 1
        end
    end
    return nbincreased
end
# display(puzzle1(numbers))

function puzzle2(numbers)
    nbincreased = 0
    for i in 1:length(numbers)-3
        if sum(numbers[i:i+2]) < sum(numbers[i+1:i+3])
            nbincreased += 1
        end
    end
    return nbincreased
end
display(puzzle2(numbers))
