using DelimitedFiles

# Input has been modified with emacs macro to add spaces betwwen bits
filename = "./input"

##############################################################################

function bintodec(binvec)
    sum([val * 2^(i-1) for (i, val) in enumerate(reverse(binvec))])
end


function puzzle1(filename)
    # Read data
    data = readdlm(filename, Bool)
    nblines = size(data, 1)

    # Compute gamma rate
    γ = [(sum(col) < nblines/2 ? 0 : 1) for col in eachcol(data)]

    # Compute epsilon rate
    ϵ = 1 .- γ

    return bintodec(γ) * bintodec(ϵ)
end
display(puzzle1(filename))

##############################################################################

function computegas(data::AbstractMatrix, operator::Function)
    nblines = size(data, 1)
    gaslines = collect(1:nblines)
    j = 1
    while length(gaslines) > 1
        dominant = operator(sum(data[gaslines,j]), length(gaslines)/2) ? 0 : 1
        linesmatching = findall(x->(x == dominant), data[gaslines,j])
        gaslines = gaslines[linesmatching]
        display
        j += 1
    end
    return gaslines[1]
end

function puzzle2(filename)
    # Read data
    data = readdlm(filename, Bool)
    nblines = size(data, 1)

    # Compute O2
    o2idx = computegas(data, <)
    o2val = bintodec(data[o2idx,:])

    # Compute CO2
    co2idx = computegas(data, >=)
    co2val = bintodec(data[co2idx,:])

    return o2val * co2val
end
display(puzzle2(filename))
# puzzle2("./example")
