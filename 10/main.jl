using Statistics # for median

# Globals constants
const openings = ['(', '[', '{', '<']
const closings = [')', ']', '}', '>']
const values = [3, 57, 1197, 25137]

function char2idx(char::Char, list::Vector{Char}) ::Int
    return findfirst(==(char), list)
end

function puzzle1(filename::String) ::Int
    # Read data
    lines = readlines(filename)

    # Loop on lines
    wrongchars = Vector{Char}()
    for line in lines
        l = [only(c) for c in split(line, "")]
        open = Vector{Char}()
        for c in l
            if c in openings
                push!(open, c)
            else
                lastopen = pop!(open)
                # if current closing char does not correspond to last opening char
                if !(char2idx(c, closings) == char2idx(lastopen, openings))
                    # save wrong char and leave line
                    push!(wrongchars, c)
                    break
                end
            end
        end
    end

    # Compute and return score
    return sum([values[char2idx(char, closings)] for char in wrongchars])
end
# display(puzzle1("./example")) # expected 26397
# display(puzzle1("./input"))



##############################################################################


# Compute score for one set of remaining unclosed chars
function computescore(chars::Vector{Char}) ::Int
    score = 0
    for c in chars
        score *= 5
        score += char2idx(c, openings)
    end
    return score
end

function puzzle2(filename::String) ::Int
    # Read data
    lines = readlines(filename)

    # Loop on lines
    scores = Vector{Int}()
    for line in lines
        l = [only(c) for c in split(line, "")]
        open = Vector{Char}()
        corrupted = false
        for c in l
            if c in openings
                push!(open, c)
            else
                lastopen = pop!(open)
                # if current closing char does not correspond to last opening char
                if !(char2idx(c, closings) == char2idx(lastopen, openings))
                    corrupted = true
                    break
                end
            end
        end
        if !corrupted
            push!(scores, computescore(reverse(open)))
        end
    end

    # Return score
    # display(scores)
    return median(scores)
end
display(puzzle2("./example")) # expected 288957
display(puzzle2("./input"))
