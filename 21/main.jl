
function rolldie(die, nbrolled)
    nbrolled[1] += 1
    ret = die[1]
    die[1] = (die[1] < 1000) ? die[1] + 1 : 1
    return ret
end

function puzzle(pos1::Int, pos2::Int)
    # Put in 1-element array to be passed as reference
    die = [1]
    nbrolled = [0]

    # Init
    score1 = 0
    score2 = 0

    # Main loop
    while max(score1, score2) < 1000
        # Player 1
        for _ in 1:3
            pos1 += rolldie(die, nbrolled)
        end
        pos1 = rem(pos1-1, 10) + 1
        score1 += pos1
        score1 >= 1000 && break
        # Player 2
        for _ in 1:3
            pos2 += rolldie(die, nbrolled)
        end
        pos2 = rem(pos2-1, 10) + 1
        score2 += pos2
    end

    return min(score1, score2) * nbrolled[1]
end
display(puzzle(4, 8)) # example
display(puzzle(2, 1)) # input
