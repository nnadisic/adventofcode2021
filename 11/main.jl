
function getadjacent(i::Int, j::Int, grid::Matrix{Int})
    maxi, maxj = size(grid)
    return (max(1,i-1):min(maxi,i+1)), (max(1,j-1):min(maxj,j+1))
end

function puzzle1(filename::String, nbsteps::Int) ::Int
    # Parse input
    lines = readlines(filename)
    grid = zeros(Int, length(lines), length(lines[1]))
    for (i, l) in enumerate(lines)
        grid[i,:] .= [parse(Int, x) for x in l]
    end

    # Loop on steps
    nbflashes = 0
    nbrow, nbcol = size(grid)
    hasflashed = zeros(Bool, nbrow, nbcol)
    for step in 1:nbsteps
        grid .+= 1
        # for one step do several passes on the grid until no more flashes
        while true
            newflash = false # count flashes in this pass
            for i in 1:nbrow
                for j in 1:nbcol
                    if grid[i,j] > 9 && hasflashed[i,j] == 0
                        newflash = true
                        hasflashed[i,j] = 1
                        adji, adjj = getadjacent(i, j, grid)
                        grid[adji, adjj] .+= 1
                    end
                end
            end
            if !newflash
                break
            end
        end
        nbflashes += count(hasflashed .== 1)
        grid[findall(hasflashed .== 1)] .= 0
        hasflashed .= 0
    end

    # Return
    return nbflashes
end
# display(puzzle1("./example", 100)) # should give 1656
# display(puzzle1("./input", 100))


function puzzle2(filename::String) ::Int
    # Parse input
    lines = readlines(filename)
    grid = zeros(Int, length(lines), length(lines[1]))
    for (i, l) in enumerate(lines)
        grid[i,:] .= [parse(Int, x) for x in l]
    end

    # Loop on steps
    nbflashes = 0
    nbrow, nbcol = size(grid)
    hasflashed = zeros(Bool, nbrow, nbcol)
    step = 0
    while true
        step += 1
        grid .+= 1
        # for one step do several passes on the grid until no more flashes
        while true
            newflash = false # count flashes in this pass
            for i in 1:nbrow
                for j in 1:nbcol
                    if grid[i,j] > 9 && hasflashed[i,j] == 0
                        newflash = true
                        hasflashed[i,j] = 1
                        adji, adjj = getadjacent(i, j, grid)
                        grid[adji, adjj] .+= 1
                    end
                end
            end
            if !newflash
                break
            end
        end
        nbflashes += count(hasflashed .== 1)
        grid[findall(hasflashed .== 1)] .= 0
        if sum(hasflashed) == length(hasflashed)
            break
        end
        hasflashed .= 0
    end

    # Return
    return step
end
display(puzzle2("./example")) # should give 195
display(puzzle2("./input"))
