using SparseArrays

##############################################################################

function puzzle1(filename)
    # Parse data
    file = open(filename, "r")
    lines = readlines(file)
    nblines = length(lines)
    x1, y1, x2, y2 = [zeros(Int, nblines) for _ in 1:4]
    for (i, line) in enumerate(lines)
        origin, destination = split(line, " -> ")
        # Add +1 because Julia arrays begin at 1 (input coordinates at 0)
        x1[i], y1[i] = [parse(Int, val)+1 for val in split(origin, ",")]
        x2[i], y2[i] = [parse(Int, val)+1 for val in split(destination, ",")]
    end

    # Build ocean floor matrix
    xmax = max(maximum(x1), maximum(x2))
    ymax = max(maximum(y1), maximum(y2))
    floor = spzeros(Int, xmax, ymax)
    for i in 1:nblines
        if (x1[i] == x2[i] || y1[i] == y2[i])
            xbegin = min(x1[i], x2[i])
            xend = max(x1[i], x2[i])
            ybegin = min(y1[i], y2[i])
            yend = max(y1[i], y2[i])
         floor[xbegin:xend,ybegin:yend] .+= 1
        end
    end

    # Count
    nboverlap = count(floor .>= 2)

    return nboverlap
end
# display(puzzle1("./example"))
# display(puzzle1("./input"))

##############################################################################

function puzzle2(filename)
    # Parse data
    file = open(filename, "r")
    lines = readlines(file)
    nblines = length(lines)
    x1, y1, x2, y2 = [zeros(Int, nblines) for _ in 1:4]
    for (i, line) in enumerate(lines)
        origin, destination = split(line, " -> ")
        # Add +1 because Julia arrays begin at 1 (input coordinates at 0)
        x1[i], y1[i] = [parse(Int, val)+1 for val in split(origin, ",")]
        x2[i], y2[i] = [parse(Int, val)+1 for val in split(destination, ",")]
    end

    # Build ocean floor matrix
    xmax = max(maximum(x1), maximum(x2))
    ymax = max(maximum(y1), maximum(y2))
    floor = zeros(Int, xmax, ymax)
    for i in 1:nblines
        if (x1[i] == x2[i] || y1[i] == y2[i])
            xbegin = min(x1[i], x2[i])
            xend = max(x1[i], x2[i])
            ybegin = min(y1[i], y2[i])
            yend = max(y1[i], y2[i])
            floor[xbegin:xend,ybegin:yend] .+= 1
        # Now add diagonals
        elseif abs(x1[i] - x2[i]) == abs(y1[i] - y2[i])
            if x2[i] >= x1[i]
                if y2[i] >= y1[i]
                    for j in 0:(x2[i]-x1[i])
                        floor[x1[i]+j, y1[i]+j] += 1
                    end
                else
                    for j in 0:(x2[i]-x1[i])
                        floor[x1[i]+j, y1[i]-j] += 1
                    end
                end
            else
                if y2[i] >= y1[i]
                    for j in 0:(x1[i]-x2[i])
                        floor[x1[i]-j, y1[i]+j] += 1
                    end
                else
                    for j in 0:(x1[i]-x2[i])
                        floor[x1[i]-j, y1[i]-j] += 1
                    end
                end
            end
        end
    end
    # display(floor')

    # Count
    nboverlap = count(floor .>= 2)

    return nboverlap
end
display(puzzle2("./example"))
display(puzzle2("./input"))
