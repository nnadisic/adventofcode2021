using Combinatorics

# Globals
global chars = ["a","b","c","d","e","f","g"]

# Constant matrix representing which segments are active in which digit
# Each row is a segment from a to g, each column is a digit from 0 to 9
#                 0 1 2 3 4 5 6 7 8 9
global mapping = [1 0 1 1 0 1 1 1 1 1 # a
                  1 0 0 0 1 1 1 0 1 1 # b
                  1 1 1 1 1 0 0 1 1 1 # c
                  0 0 1 1 1 1 1 0 1 1 # d
                  1 0 1 0 0 0 1 0 1 0 # e
                  1 1 0 1 1 1 1 1 1 1 # f
                  1 0 1 1 0 1 1 0 1 1 # g
                  ]

function rawsegment2binvector(segment)
    return [occursin(char, segment) ? 1 : 0 for char in chars]
end

function findperm(input, output)
    goodperm = []
    for perm in permutations(1:7)
        isvalid = true
        for segment in [rawsegment2binvector(s) for s in [input; output]]
            segmentexists = false
            for col in eachcol(mapping)
                if segment[perm] == col
                    segmentexists = true
                    break
                end
            end
            if !segmentexists
                isvalid=false
                break
            end
        end
        if isvalid
            goodperm = perm
            break
        end
    end
    return goodperm
end

function segment2number(segment, perm)
    vec = rawsegment2binvector(segment)[perm]
    for j in 1:size(mapping, 2)
        if vec == mapping[:,j]
            return j-1
        end
    end
    return -1
end

function puzzle(filename)
    # Read data
    lines = readlines(filename)

    # Main loop to translate segments into numbers
    leftnumbers = []
    rightnumbers = []
    for line in lines
        inputs, outputs = [split(x) for x in split(line, "|")]
        perm = findperm(inputs, outputs)
        inputs_int = [segment2number(s, perm) for s in inputs]
        push!(leftnumbers, inputs_int)
        outputs_int = [segment2number(s, perm) for s in outputs]
        push!(rightnumbers, outputs_int)
    end

    # display(rightnumbers)

    # Puzzle 1
    # Find occurences of 1, 4, 7, and 8
    concatright = reduce(vcat, rightnumbers)
    out1 = 0
    for number in [1,4,7,8]
        out1 += count(==(number), concatright)
    end

    # Puzzle 2
    # Find all right numbers and sum them
    out2 = sum([sum([1000, 100, 10, 1] .* n) for n in rightnumbers])

    # Return
    return out1, out2
end
display(puzzle("./example"))
display(puzzle("./input"))
