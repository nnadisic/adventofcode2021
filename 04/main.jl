using DelimitedFiles

##############################################################################

function puzzle1(filename)
    # Read data
    rawdata = read(filename, String)
    data = split(rawdata, "\n\n")
    pulls = [parse(Int, x) for x in split(popfirst!(data), ",")]
    boards = [readdlm(IOBuffer(b), Int) for b in data]

    # Init markers
    markers = [ones(Bool, size(b)) for b in boards]

    # Play!
    winningboard_idx = 0
    winningpull = 0
    while true
        pull = popfirst!(pulls)
        for (idx, board) in enumerate(boards)
            match = findall(x->x==pull, board) # returns vector of cartesian indices
            markers[idx][match] .= 0
            if !iszero([iszero(row) for row in eachrow(markers[idx])])
                return pull * sum(board .* markers[idx])
            end
        end
    end
end
display(puzzle1("./example"))
display(puzzle1("./input"))

##############################################################################

function puzzle2(filename)
    # Read data
    rawdata = read(filename, String)
    data = split(rawdata, "\n\n")
    pulls = [parse(Int, x) for x in split(popfirst!(data), ",")]
    boards = [readdlm(IOBuffer(b), Int) for b in data]

    # Init markers
    markers = [ones(Bool, size(b)) for b in boards]
    winningboards = ones(Bool, length(boards))

    # Play!
    winningboard_idx = 0
    winningpull = 0
    while true
        pull = popfirst!(pulls)
        for (idx, board) in enumerate(boards)
            match = findall(x->x==pull, board) # returns vector of cartesian indices
            markers[idx][match] .= 0
            if (!iszero([iszero(row) for row in eachrow(markers[idx])])
                || !iszero([iszero(col) for col in eachcol(markers[idx])]))
                winningboards[idx] = 0
                if sum(winningboards) == 0
                    return pull * sum(boards[idx] .* markers[idx])
                end
            end
        end
    end
end
display(puzzle2("./example"))
display(puzzle2("./input"))
